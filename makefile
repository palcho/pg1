CC = gcc
CFLAGS = -flto -Wall -Wno-missing-braces -Wl,-rpath=./SDL2/lib
LFLAGS = -lm -lSDL2
RFLAGS = -O3
GFLAGS = -Og -ggdb
IFLAGS = -I./SDL2/include -L./SDL2/lib
WFLAGS = -lmingw32 -lSDL2main
SOURCE = $(addprefix src/, pg.c mtrx.c graphic.c glad.c utils.c mechanics.c)
HEADER = $(addprefix src/, mtrx.h graphic.h glad.h utils.h mechanics.h)
.PHONY: linux windows linuxdb windowsdb clean

linux: pg
linuxdb: pgdb
windows: pg.exe
windowsdb: pgdb.exe

pg: $(SOURCE) $(HEADER)
	$(CC) $(SOURCE) $(RFLAGS) $(CFLAGS) $(IFLAGS) $(LFLAGS) -o $@

pgdb: $(SOURCE) $(HEADER)
	$(CC) $(SOURCE) $(GFLAGS) $(CFLAGS) $(IFLAGS) $(LFLAGS) -o $@

pg.exe: $(SOURCE) $(HEADER)
	$(CC) $(SOURCE) $(RFLAGS) $(CFLAGS) $(IFLAGS) $(WFLAGS) $(LFLAGS) -o $@

pgdb.exe: $(SOURCE) $(HEADER)
	$(CC) $(SOURCE) $(GFLAGS) $(CFLAGS) $(IFLAGS) $(WFLAGS) $(LFLAGS) -o $@

tags: $(SOURCE) $(HEADER)
	ctags -R

clean:
	-rm -f pg pgdb pg.exe pgdb.exe *.s

#-Wl,-subsystem,windows : suprime o prompt de comando ao abrir o executável
# tirar HAS_TERMINAL da gflag se usar esta opção
