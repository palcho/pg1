#version 120

attribute vec3 vertex;
attribute vec3 normal;

uniform mat4 vertex_mtrx;
uniform mat4 normal_mtrx;
uniform mat4 view_mtrx;
uniform mat4 proj_mtrx;

varying vec3 _normal;
varying vec3 frag_pos;

void main () {
	gl_Position = proj_mtrx * view_mtrx * vertex_mtrx * vec4(vertex, 1.0);
	frag_pos = vec3(vertex_mtrx * vec4(vertex, 1.0));
	_normal = normalize(mat3(normal_mtrx) * normal);
}
