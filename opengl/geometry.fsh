#version 120

varying vec3 _normal;
varying vec3 frag_pos;

uniform vec3 object_color;
uniform vec3 light_pos;
uniform vec4 material;
uniform vec3 view_pos;

#define ke material.x
#define kd material.y
#define ks material.z
#define alpha material.w

void main () {
	vec3 normal = _normal;
	// dot(_normal, view_pos - frag_pos) < 0.0 ? -_normal : _normal;
	// ambient lighting
	vec3 light_color = vec3(1.0, 1.0, 1.0);
	vec3 ambient =  ke * light_color;
	// diffuse lighting
	vec3 light_dir = normalize(light_pos - frag_pos);
	vec3 diffuse = kd * max(dot(normal, light_dir), 0.0) * light_color;
	// specular lighting
	vec3 view_dir = normalize(view_pos - frag_pos);
	vec3 reflect_dir = reflect(-light_dir, normal);
	vec3 specular = ks * pow(max(dot(view_dir, reflect_dir), 0.0), alpha) * light_color;
	// Phong lighting model
	vec3 phong = (ambient + diffuse + specular) * object_color;
	gl_FragColor = vec4(phong, 1.0);
}
