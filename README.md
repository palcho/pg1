---
Projeto I de Processamento Gráfico
---

# Conteúdo da Pasta

* pg e pg.exe são os executáveis já compilados para Linux e Windows, respectivamente
* O código fonte do projeto está na pasta src (glad.c e glad.h não são meus)
* Os shaders estão na pasta opengl
* A pasta SDL2 contém os headerfiles e arquivos .so e .a desta biblioteca; SDL2.dll é seu dll para Windows
* makefile serve para compilar o projeto no Linux, pelo comando 'make', ou no Windows, pelo comando 'make windows' (no Windows só compilou com make.exe e cmder com bash; no Linux compila normalmente).
* tags é um índice gerado pelo comando ctags, que é usado pelo editor de texto Vim
* os arquivos com cenas e objetos ficam na pasta scenes

# Instruções de Uso

### Modo Movimentação de câmera (Modo Normal)

* W A S D R F: translação da câmera
* Movimento do mouse: rotação da câmera
* Clicar e segurar: seleciona um objeto, que passa a acompanhar a translação da câmera
* Segurar Barra de Espaço: seleciona um objeto e passa para o modo de manipulação


### Modo Manipulação de Objeto

* W A S D R F: translação do objeto
* Movimento do mouse: rotação do objeto pelos eixos Y e X
* segurar E: rotação passa a ser pelos eixos Z e X
* segurar C: rotação passa a ser pelos eixos Z e Y
* segurar V: movimento do mouse aumenta (cima/esquerda) ou diminu (baixo/esquerda) a escala do objeto
* Soltar Barra de Espaço: volta ao modo normal


### Independentemente do Modo

* Q: sai do programa
* P: desativa V-Sync e mostra os fps; aperte de novo para reverter
* segurar o Shift Esquerdo: movimentos comandados por W A S D R F ficam mais rápidos


Felipe Guerra - fcgr
