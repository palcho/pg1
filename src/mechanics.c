#include <SDL2/SDL.h>
#include "mechanics.h"

#define DELTA_ROTATION 0.002f
#define DELTA_STRAIGHT 0.002f
#define MOUSE_MOVE_ADJUST 0.1f

enum movement_flags {
	MOVE_FORWARD  = 0x01,
	MOVE_BACKWARD = 0x02,
	MOVE_LEFT     = 0x04,
	MOVE_RIGHT    = 0x08,
	TURN_LEFT     = 0x10,
	TURN_RIGHT    = 0x20,
	LOOK_UP       = 0x40,
	LOOK_DOWN     = 0x80,
	USING_MOUSE   = 0x0100,
	GO_FASTER     = 0x0200,
	MOVE_UP       = 0x0400,
	MOVE_DOWN     = 0x0800,
	Z_Y_ROTATION  = 0x1000,
	Z_X_ROTATION  = 0x2000,
	SCALING       = 0x4000,
	
};

uint16_t mflag = 0;
Model *grabbed_model = NULL;
Vect dr = {}, ds = {}; // delta rotation, delta space

void read_inputs (Timer *timer) {
	SDL_Event event;
	//static Vect dm = {}; // delta mouse

	dr = *mul_sv(0.4f, &dr);
	////dr = *mul_sv((expf(0.06f*logf(0.4f) * timer->dt) - 1.0f) / (0.06f*logf(0.4f)), &dm);
	//Vect *new_dm = mul_sv(powf(0.01f, timer->dt), &dm);
	//dr = *sub_vv(&dm, new_dm);
	//dm = *new_dm;
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_QUIT:
				gflag |= QUIT;
				break;
			case SDL_MOUSEMOTION:
				//printf("mouse motion\n");
				if (grabbed_model && gflag & MOVING_MODEL && mflag & Z_X_ROTATION) {
					//dm.z = dr.z += -DELTA_ROTATION * event.motion.xrel;
					dr.z += -DELTA_ROTATION * event.motion.xrel;
				} else {
					//dm.y = dr.y += -DELTA_ROTATION * event.motion.xrel;
					dr.y += -DELTA_ROTATION * event.motion.xrel;
				}
				if (grabbed_model && gflag & MOVING_MODEL && mflag & Z_Y_ROTATION) {
					//dm.z = dr.z += -DELTA_ROTATION * event.motion.yrel;
					dr.z += -DELTA_ROTATION * event.motion.yrel;
				} else {
					//dm.x = dr.x += -DELTA_ROTATION * event.motion.yrel;
					dr.x += -DELTA_ROTATION * event.motion.yrel;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (!grabbed_model) gflag |= SELECT_MODEL;
				gflag |= WALKING_MODEL;
				break;
			case SDL_MOUSEBUTTONUP:
				if (~gflag & MOVING_MODEL) grabbed_model = NULL;
				gflag &= ~WALKING_MODEL;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
					case SDLK_q:
						gflag |= QUIT;
						break;
					case SDLK_p:
						gflag ^= SHOW_FPS;
						SDL_GL_SetSwapInterval(gflag & SHOW_FPS ? 0 : 1);
						break;
					case SDLK_LSHIFT:
						mflag |= GO_FASTER;
						break;
					case SDLK_SPACE:
						if (!grabbed_model) gflag |= SELECT_MODEL;
						gflag |= MOVING_MODEL;
						break;
					case SDLK_e:
						mflag |= Z_X_ROTATION;
						break;
					case SDLK_c:
						mflag |= Z_Y_ROTATION;
						break;
					case SDLK_v:
						mflag |= SCALING;
						break;
					case SDLK_w:
						ds.z = DELTA_STRAIGHT;
						mflag |= MOVE_FORWARD;
						break;
					case SDLK_s:
						ds.z = -DELTA_STRAIGHT;
						mflag |= MOVE_BACKWARD;
						break;
					case SDLK_d:
						ds.x = DELTA_STRAIGHT;
						mflag |= MOVE_RIGHT;
						break;
					case SDLK_a:
						ds.x = -DELTA_STRAIGHT;
						mflag |= MOVE_LEFT;
						break;
					case SDLK_r:
						ds.y = DELTA_STRAIGHT;
						mflag |= MOVE_UP;
						break;
					case SDLK_f:
						ds.y = -DELTA_STRAIGHT;
						mflag |= MOVE_DOWN;
						break;
					default: break;
				}
				break;
			case SDL_KEYUP:
				switch (event.key.keysym.sym) {
					case SDLK_LSHIFT:
						mflag &= ~GO_FASTER;
						break;
					case SDLK_SPACE:
						if (~gflag & WALKING_MODEL) grabbed_model = NULL;
						gflag &= ~MOVING_MODEL;
						break;
					case SDLK_e:
						mflag &= ~Z_X_ROTATION;
						break;
					case SDLK_c:
						mflag &= ~Z_Y_ROTATION;
						break;
					case SDLK_v:
						mflag &= ~SCALING;
						break;
					case SDLK_w:
						ds.z = mflag & MOVE_BACKWARD ? -DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_FORWARD;
						break;
					case SDLK_s:
						ds.z = mflag & MOVE_FORWARD ? DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_BACKWARD;
						break;
					case SDLK_d:
						ds.x = mflag & MOVE_LEFT ? -DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_RIGHT;
						break;
					case SDLK_a:
						ds.x = mflag & MOVE_RIGHT ? DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_LEFT;
						break;
					case SDLK_r:
						ds.y = mflag & MOVE_DOWN ? -DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_UP;
						break;
					case SDLK_f:
						ds.y = mflag & MOVE_UP ? DELTA_STRAIGHT : 0.0f;
						mflag &= ~MOVE_DOWN;
						break;
					default: break;
				}
				break;
			default: break;
		}
	}
	//printf("dt = %u, dr = (%f, %f, %f), dm = (%f, %f, %f)\n", timer->dt, dr.x, dr.y, dr.z, dm.x, dm.y, dm.z);
}

void update_camera_position (Camera *camera, Timer *timer) {
	Vect front = *unit_vect(sub_vv(&camera->aim, &camera->eye));
	Vect right = *cross(&front, &camera->up);
	float faster = mflag & GO_FASTER ? 3.0f : 1.0f;
	float multiplier =  faster * timer->dt;

	if (grabbed_model && gflag & MOVING_MODEL) {
		camera->ds = *mul_sv(0.9f, &camera->ds);
		camera->dr = *mul_sv(0.6f, &camera->dr);
		if (mflag & SCALING) {
			grabbed_model->scale += dr.x - dr.y;
		} else {
			grabbed_model->orientation = *unit_quat(mul_qq(
				mul_qq(quat_from_vect(&front, dr.z), quat_from_vect(&right, -dr.x)),
				mul_qq(quat_from_vect(&camera->up, -dr.y), &grabbed_model->orientation)));
			//printf("%f %f %f %f\n", grabbed_model->orientation.x, grabbed_model->orientation.y, grabbed_model->orientation.z, grabbed_model->orientation.w);
		}
		grabbed_model->translation = *add_vv (
			add_vv(&grabbed_model->translation, mul_sv(ds.y * multiplier, &camera->up)),
			add_vv(mul_sv(ds.x * multiplier, &right), mul_sv(ds.z * multiplier, &front)));
	} else {
		if (grabbed_model && gflag & WALKING_MODEL) {
			grabbed_model->translation = *add_vv (
				add_vv(&grabbed_model->translation, mul_sv(ds.y * multiplier, &camera->up)),
				add_vv(mul_sv(ds.x * multiplier, &right), mul_sv(ds.z * multiplier, &front)));
		}
		camera->ds = ds;
		camera->dr = dr;
	}

	Quat rotation = *mul_qq(quat_from_vect(&camera->up, camera->dr.y), quat_from_vect(&right, camera->dr.x));

	camera->eye = *add_vv (
		add_vv(&camera->eye, mul_sv(camera->ds.y * multiplier, &camera->up)),
		add_vv(mul_sv(camera->ds.x * multiplier, &right), mul_sv(camera->ds.z * multiplier, &front)));
	camera->aim = *add_vv(&camera->eye, mul_qviq(&rotation, &front));
	camera->viewm = *view_mtrx(&camera->eye, &camera->aim, &camera->up);

}
