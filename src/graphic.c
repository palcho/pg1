#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <float.h>
#include "graphic.h"
#include "mechanics.h"

void count_instances (int *counters, char **instances, int num_instances, char *text);
int count_lines (int *count, char *text);

void create_opengl_context (Screen *screen, Scene *scene, uint32_t flags) {
	assertq(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) >= 0);
#if defined(WINDOWS) || defined(LINUX) || defined(MAC)
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE) >= 0);
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) >= 0);
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1) >= 0);
	SDL_SetRelativeMouseMode(SDL_TRUE);
#elif defined(ANDROID) || defined(IOS)
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES) >= 0);
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) >= 0);
	assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) >= 0);
#endif
	gflag |= flags;
	assertq(SDL_GetCurrentDisplayMode(0, &screen->display) >= 0);
	if (gflag & FULLSCREEN_ON) {
		scene->res.width = screen->display.w;
		scene->res.height = screen->display.h;
	} else {
		screen->display.w = scene->res.width;
		screen->display.h = scene->res.height;
	}
	printf("display: %dx%d\nrefresh rate = %d Hz\n", screen->display.w, screen->display.h, screen->display.refresh_rate);
	screen->sizef.w = (float)screen->display.w;
	screen->sizef.h = (float)screen->display.h;
	screen->window = SDL_CreateWindow(scene->filename, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screen->display.w, screen->display.h,
		SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | (gflag & FULLSCREEN_ON ? SDL_WINDOW_FULLSCREEN : 0));
	assertq(screen->window != NULL);
	screen->context = SDL_GL_CreateContext(screen->window);
	assertq(screen->context != 0);
#if defined(WINDOWS) || defined(LINUX) || defined(MAC)
	gladLoadGLLoader(SDL_GL_GetProcAddress);
	assertq(GLAD_GL_ARB_framebuffer_object || (GLAD_GL_EXT_framebuffer_object && GLAD_GL_EXT_packed_depth_stencil) || GLAD_GL_VERSION_3_0);
	if (GLAD_GL_ARB_vertex_array_object || GLAD_GL_VERSION_3_0)
		gflag |= HAS_VAO;
	printf("%d %d %d %d\n", GLAD_GL_VERSION_3_0, GLAD_GL_ARB_vertex_array_object, GLAD_GL_ARB_framebuffer_object, GLAD_GL_EXT_framebuffer_object);
#elif defined(ANDROID) || defined(IOS)
	gladLoadGLES2Loader(SDL_GL_GetProcAddress);
	assertq((GLAD_GL_OES_depth_texture && GLAD_GL_OES_packed_depth_stencil) || GLAD_GL_ES_VERSION_3_0);
	if (GLAD_GL_OES_vertex_array_object || GLAD_GL_ES_VERSION_3_0)
		gflag |= HAS_VAO;
	printf("%d %d %d\n", GLAD_GL_ES_VERSION_3_0, GLAD_GL_OES_vertex_array_object, GLAD_GL_OES_depth_texture);
#endif
	gflag |= OPENGL_INITIALIZED;
	printf("vendor: %s\nrenderer: %s\nversion: %s\n",\
		glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));
	SDL_GL_SetSwapInterval(gflag & VSYNC_ON ? 1 : 0);
#ifdef ANDROID
	SDL_SetHint(SDL_HINT_ANDROID_SEPARATE_MOUSE_AND_TOUCH, "1");
#endif
}

void program_from_shader_source (Renderer *renderer, int program_index, const char *vertsh_file, const char *fragsh_file) {
	GLint status;
	renderer->program_index = program_index;
	renderer->program = glCreateProgram();
	assertq(renderer->program);
	renderer->vertex_shader = shader_from_sourcefile(GL_VERTEX_SHADER, vertsh_file);
	renderer->fragment_shader = shader_from_sourcefile(GL_FRAGMENT_SHADER, fragsh_file);
	assertq(renderer->vertex_shader && renderer->fragment_shader);

	GL(glAttachShader(renderer->program, renderer->vertex_shader));
	GL(glAttachShader(renderer->program, renderer->fragment_shader));
	GL(glLinkProgram(renderer->program));
	GL(glDetachShader(renderer->program, renderer->vertex_shader));
	GL(glDetachShader(renderer->program, renderer->fragment_shader));
	GL(glDeleteShader(renderer->vertex_shader));
	GL(glDeleteShader(renderer->fragment_shader));

	GL(glGetProgramiv(renderer->program, GL_LINK_STATUS, &status));
	assertq(status == GL_TRUE);

	renderer->framebuffer = renderer->fbo = 0;
#if defined(ANDROID) || defined(IOS)
	renderer->rbo = 0;
#endif
}

void retrieve_attributes (Renderer *renderer, const char *vertex_attrib_name, const char *normal_attrib_name, const char *texture_coordinate_attrib_name) {
	renderer->attribute.vertex = glGetAttribLocation(renderer->program, vertex_attrib_name);
	assertq(renderer->attribute.vertex >= 0);

	renderer->attribute.normal = renderer->attribute.texture_coordinate = -1;
	if (normal_attrib_name != NULL) { // normals will be used
		renderer->attribute.normal = glGetAttribLocation(renderer->program, normal_attrib_name);
		assertq(renderer->attribute.normal >= 0);
	}
	if (texture_coordinate_attrib_name != NULL) { // normals and textures will be used
		renderer->attribute.texture_coordinate = glGetAttribLocation(renderer->program, texture_coordinate_attrib_name);
		assertq(renderer->attribute.texture_coordinate >= 0);
	}
}

void retrieve_uniforms (Renderer *renderer, int count, ...) {
	GL(glUseProgram(renderer->program));
	va_list uniform_names;
	va_start(uniform_names, count);
	for (int i = 0; i < count; i++) {
		renderer->uniform[i] = glGetUniformLocation(renderer->program, va_arg(uniform_names, const char *));
		assertq(renderer->uniform[i] >= 0);
	}
	va_end(uniform_names);
	for (int i = count; i < MAX_UNIFORMS; i++)
		renderer->uniform[i] = -1;
}

void retrieve_buffer_objects (Model *model, int num_vaos, ...) { // cast to (Mesh *) if it's not a mesh
	va_list program_indices;
	va_start(program_indices, num_vaos);
	for (int i = 0; i < num_vaos; i++) {
		if (gflag & HAS_VAO) {
			GL(glGenVertexArrays(1, &model->vao[va_arg(program_indices, int)]));
		} else { // mark as user of that program
			model->vao[va_arg(program_indices, int)] = 1;
		}
	}
	va_end(program_indices);
	// vertex array data
	GL(glGenBuffers(1, &model->vbo));
	GL(glBindBuffer(GL_ARRAY_BUFFER, model->vbo));
	GL(glBufferData(GL_ARRAY_BUFFER, model->mesh.num_vertices * (2*sizeof(Vect) + sizeof(Vec2)), model->mesh.vertices.vertex, GL_STATIC_DRAW));
	// element array data, if used
	if (model->mesh.face != NULL) {
		GL(glGenBuffers(1, &model->ebo));
		GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo));
		GL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, model->mesh.num_faces * 3*sizeof(FaceIndices),
			model->mesh.face, GL_STATIC_DRAW));
	}
}

void vertex_attribute_data (const Model *model, const Renderer *renderer) {
	GL(glUseProgram(renderer->program));
	if (gflag & HAS_VAO) GL(glBindVertexArray(model->vao[renderer->program_index]));
	GL(glBindBuffer(GL_ARRAY_BUFFER, model->vbo));
	GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo));
	// vertex attribute
	GL(glEnableVertexAttribArray(renderer->attribute.vertex));
	GL(glVertexAttribPointer(renderer->attribute.vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vect), (void *)0));
		// edges attributes
	if (renderer->attribute.normal >= 0) { // if normals will be used
		GL(glEnableVertexAttribArray(renderer->attribute.normal));
		GL(glVertexAttribPointer(renderer->attribute.normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vect),
			(void *)(model->mesh.num_vertices * sizeof(Vect))));
	}
	if (renderer->attribute.texture_coordinate >= 0) { // if textures will be used
		GL(glEnableVertexAttribArray(renderer->attribute.texture_coordinate));
		GL(glVertexAttribPointer(renderer->attribute.texture_coordinate, 3, GL_FLOAT, GL_FALSE, sizeof(Vec2),
			(void *)(2*model->mesh.num_vertices * sizeof(Vect))));
	}
	if (gflag & HAS_VAO) GL(glBindVertexArray(0));
}

void retrieve_framebuffer (Renderer *renderer) {
	GL(glGenFramebuffers(1, &renderer->fbo));
	GL(glBindFramebuffer(GL_FRAMEBUFFER, renderer->fbo));
	GL(glGenTextures(1, &renderer->framebuffer));
	GL(glBindTexture(GL_TEXTURE_2D, renderer->framebuffer));
	GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
	GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
	GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
	GL(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, renderer->viewport.w, renderer->viewport.h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL));
	GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, renderer->framebuffer, 0));
#if defined(ANDROID) || defined(IOS) // OpenGLES framebuffers need a color buffer to be complete
	GL(glGenRenderbuffers(1, &renderer->rbo));
	GL(glBindRenderbuffer(GL_RENDERBUFFER, renderer->rbo));
	GL(glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB565, renderer->viewport.w, renderer->viewport.h));
	GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderer->rbo));
#elif defined(WINDOWS) || defined(LINUX) || defined(MAC) // OpenGL framebuffers don't
	GL(glDrawBuffer(GL_NONE));
	GL(glReadBuffer(GL_NONE));
#endif
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assertq(status == GL_FRAMEBUFFER_COMPLETE);
	GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

GLuint shader_from_sourcefile (GLenum type, const char *path) {
	GLint nread, status;
	GLuint shader = 0;

	nread = read_file(&gbuffer, SHADER_DIR, path);
	asserth(nread > 0, shader_read_error);
	shader = glCreateShader(type);
	GL(glShaderSource(shader, 1, (const char **)&gbuffer.data, &nread));
	GL(glCompileShader(shader));
	GL(glGetShaderInfoLog(shader, gbuffer.size, &nread, gbuffer.data));
	gbuffer.data[nread] = '\0';

	GL(glGetShaderiv(shader, GL_COMPILE_STATUS, &status));
	if (status != GL_TRUE) {
		if (gflag & HAS_TERMINAL) {
			fprintf(stderr, "shader compilation error at file '%s': %s\n", path, gbuffer.data);
		} else {
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, path, gbuffer.data, NULL);
		}
		goto shader_compile_error;
	}
	return shader;

shader_compile_error:
	GL(glDeleteShader(shader));
shader_read_error:
	return 0;
}

void set_opengl_options (void) {
	GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
	GL(glDisable(GL_BLEND));
	GL(glEnable(GL_CULL_FACE));
	GL(glEnable(GL_DEPTH_TEST));
}

void screen_close (Screen *screen) {
	// the line below causes segmentation fault on w7 gl2.1
	//SDL_GL_DeleteContext(screen->context);
	gflag &= ~OPENGL_INITIALIZED;
	free(gbuffer.data);
	SDL_DestroyWindow(screen->window);
	SDL_Quit();
}

Model *models_from_scene (Scene *scene) {
	Model *object_model = calloc(scene->num_models, sizeof(Model));
	if (!object_model) err("failed to allocate object models: %s\n", strerror(errno));

	for (int i = 0, j = 0; i < scene->num_models; i++) {
		object_model[i].orientation.w = object_model[i].scale = 1.0f;
		mesh_from_file(&object_model[i].mesh, scene->model_info[i].mesh_filename);
		for (j = 0; j < scene->num_materials; j++) {
			if (strcmp(scene->model_info[i].material_name, scene->material[j].name) == 0) break;
		}
		if (j == scene->num_materials) {
			err("there is no material '%s' for mesh defined at line %d of file '%s'\n",
				scene->model_info[i].material_name, scene->model_info[i].file_line, scene->filename);
		}
		object_model[i].material = &scene->material[j];
	}
	return object_model;
}

void camera_from_scene_and_screen (Camera *camera, Scene *scene, Screen *screen) {
	*camera = (Camera) {
		.eye = scene->cam.eye,
		.aim = scene->cam.aim,
		.up = *unit_vect(&scene->cam.up),
		.viewm = *view_mtrx(&scene->cam.eye, &scene->cam.aim, &scene->cam.up),
		.projm = *persp_mtrx(scene->cam.focus, 100.0f, scene->cam.fov * (PI/180.0f),
			screen->sizef.w/screen->sizef.h),
		.focus = scene->cam.focus,
	};
}

enum { false, true };

void draw_models (Renderer *renderer, Camera *camera, int num_models, Model *models, Vect *light_source) {
	float *intercept_dist = alloca(num_models * sizeof(float));
	GL(glViewport(renderer->viewport.x, renderer->viewport.y, renderer->viewport.w, renderer->viewport.h));
	GL(glBindFramebuffer(GL_FRAMEBUFFER, renderer->fbo));
	if (renderer->clear_flags)
		GL(glClear(renderer->clear_flags));
	GL(glUseProgram(renderer->program));
	Mtrx ident_mtrx = { .rx = 1, .uy = 1, .fz = 1, .tw = 1 };

		if (gflag & SELECT_MODEL) printf("selecting model\n");
	if (renderer->program_index == GEOMETRY_RENDERER) {
		Mtrx normal_mtrx, vertex_mtrx, scale_mtrx = { .tw = 1.0f };
		for (int i = 0; i < num_models; i++) {
			normal_mtrx = *rotm_from_quat(&models[i].orientation);
			scale_mtrx.rx = scale_mtrx.uy = scale_mtrx.fz = models[i].scale;
			vertex_mtrx = *mul_mm ( // translation * rotation * scaling
				translate(add_vv(&models[i].translation, &models[i].mesh.baricenter), mul_mm(&normal_mtrx, &scale_mtrx)),
				translate(mul_sv(-1, &models[i].mesh.baricenter), &ident_mtrx));
			GL(glUniformMatrix4fv(renderer->uniform[0], 1, GL_FALSE, (float *)&vertex_mtrx));
			GL(glUniformMatrix4fv(renderer->uniform[1], 1, GL_FALSE, (float *)&normal_mtrx));
			GL(glUniformMatrix4fv(renderer->uniform[2], 1, GL_FALSE, (float *)&camera->viewm));
			GL(glUniformMatrix4fv(renderer->uniform[3], 1, GL_FALSE, (float *)&camera->projm));
			GL(glUniform3fv(renderer->uniform[4], 1, (float *)&models[i].material->color));
			GL(glUniform3fv(renderer->uniform[5], 1, (float *)light_source));
			GL(glUniform4fv(renderer->uniform[6], 1, (float *)&models[i].material->index));
			GL(glUniform3fv(renderer->uniform[7], 1, (float *)&camera->eye));

			GL(glBindVertexArray(models[i].vao[renderer->program_index]));
			//GL(glDrawElements(GL_TRIANGLES, models[i].mesh.num_faces*9, GL_UNSIGNED_SHORT, (void *)0));
			GL(glDrawArrays(GL_TRIANGLES, 0, models[i].mesh.num_vertices));

			if (gflag & SELECT_MODEL) {
				Vec4 vertx[8];
				vertx[0] = (Vec4){ models[i].mesh.min_corner, 1.0f };
				vertx[1] = (Vec4){ models[i].mesh.max_corner.x, models[i].mesh.min_corner.y, models[i].mesh.min_corner.z, 1.0f };
				vertx[2] = (Vec4){ models[i].mesh.min_corner.x, models[i].mesh.max_corner.y, models[i].mesh.min_corner.z, 1.0f };
				vertx[3] = (Vec4){ models[i].mesh.max_corner.x, models[i].mesh.max_corner.y, models[i].mesh.min_corner.z, 1.0f };
				vertx[4] = (Vec4){ models[i].mesh.min_corner.x, models[i].mesh.min_corner.y, models[i].mesh.max_corner.z, 1.0f };
				vertx[5] = (Vec4){ models[i].mesh.max_corner.x, models[i].mesh.min_corner.y, models[i].mesh.max_corner.z, 1.0f };
				vertx[6] = (Vec4){ models[i].mesh.min_corner.x, models[i].mesh.max_corner.y, models[i].mesh.max_corner.z, 1.0f };
				vertx[7] = (Vec4){ models[i].mesh.max_corner, 1.0f };

				Mtrx m = *mul_mm(&camera->projm, mul_mm(&camera->viewm, &vertex_mtrx)); 

				for (int j = 0; j < 8; j++) {
					vertx[j] = *mul_mv4(&m, &vertx[j]);
					vertx[j].u = *mul_sv(-1/vertx[j].w, &vertx[j].u);
				}
				FaceIndices idx[12] = { {0,1,2},{0,1,4},{0,2,4},{3,2,1},{3,1,7},{3,2,7},
					{5,1,4},{5,1,7},{5,4,7},{6,2,4},{6,2,7},{6,4,7} };

				intercept_dist[i] = FLT_MAX;
				for (int j = 0; j < 12; j++) {
					if (vertx[idx[j].v1].w < camera->focus || vertx[idx[j].v2].w < camera->focus ||
						vertx[idx[j].v3].w < camera->focus) continue;
					Vec4 a = vertx[idx[j].v1], b = vertx[idx[j].v2], c = vertx[idx[j].v3];

					//printf("%f %f %f | %f %f %f | %f %f %f\n", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z);
					int s_ab = (b.x - a.x) * (-a.y) - (b.y - a.y) * (-a.x) > 0; 
					if (((c.x - a.x) * (-a.y) - (c.y - a.y) * (-a.x) > 0) == s_ab) continue;
					if (((c.x - b.x) * (-b.y) - (c.y - b.y) * (-b.x) > 0) != s_ab) continue;
					intercept_dist[i] *= 0.9f;
					printf("mesh %d's bounding box was intercepted\n", i);
					break;
				}

				if (intercept_dist[i] < FLT_MAX) {
					for (int j = 0; j < models[i].mesh.num_vertices; j += 3) {
						Vec4 a, b, c;
						a = *mul_mv3(&m, &models[i].mesh.vertices.vertex[j]);
						b = *mul_mv3(&m, &models[i].mesh.vertices.vertex[j+1]);
						c = *mul_mv3(&m, &models[i].mesh.vertices.vertex[j+2]); 
						a.u = *mul_sv(-1/a.w, &a.u); b.u = *mul_sv(-1/b.w, &b.u); c.u = *mul_sv(-1/c.w, &c.u);

						if (a.w < camera->focus || b.w < camera->focus || c.w < camera->focus) continue;
						int s_ab = (b.x - a.x) * (-a.y) - (b.y - a.y) * (-a.x) > 0; 
						if (((c.x - a.x) * (-a.y) - (c.y - a.y) * (-a.x) > 0) == s_ab) continue;
						if (((c.x - b.x) * (-b.y) - (c.y - b.y) * (-b.x) > 0) != s_ab) continue;
						float dst_a = 1/sqrtf(a.x*a.x + a.y*a.y);
						float dst_b = 1/sqrtf(b.x*b.x + b.y*b.y);
						float dst_c = 1/sqrtf(c.x*c.x + c.y*c.y);
						float all = dst_a + dst_b + dst_c;
						float dist = a.w * dst_a/all + b.w * dst_b/all + c.w * dst_c/all;
						intercept_dist[i] = fmin(intercept_dist[i], dist);
						printf("mesh %d was intercepted indeed at distance %f: (%f, %f, %f)\n", i, dist, a.w, b.w, c.w);
						//break;
					}
				} 

			} 
		}
	}
	if (gflag & SELECT_MODEL) {
		int min_index = 0;
		for (int i = 1; i < num_models; i++) {
			if (intercept_dist[i] < intercept_dist[min_index])
				min_index = i;
		}
		if (intercept_dist[min_index] < 0.9f * FLT_MAX) {
			grabbed_model = &models[min_index];
			printf("mesh %d was selected\n", min_index);
		}
		gflag &= ~SELECT_MODEL;
	}
}

#define _ "%*[ \t]"

void scene_from_file (Scene *scene, char *scene_filename) {
	char entry[32];
	int nread, count = 0, nmatches, lineno = 1;
	char *instances[] = { "mesh", "material" };
	int instance_counters[arrsz(instances)] = {};
	void *memory;

	read_file(&gbuffer, SCENE_DIR, scene_filename);
	if (gbuffer.count <= 0) err("failed to read file %s: %s\n", scene_filename, SDL_GetError());

	count_instances(instance_counters, instances, arrsz(instances), gbuffer.data);
	memory = malloc(instance_counters[0] * sizeof(Mesh) + instance_counters[1] * sizeof(Material));
	if (!memory) err("failed to allocate memory for scene from file '%s': %s\n", scene_filename, strerror(errno));

	*scene = (Scene) {
		.filename = scene_filename,
		.model_info = memory,
		.material = memory + instance_counters[0] * sizeof(Mesh),
	};

	printf("reading %d bytes from file '%s'\n", gbuffer.count, scene_filename);
	lineno += count_lines(&count, gbuffer.data);
	while (sscanf(gbuffer.data + count, "%s%n", entry, &nread) > 0) {
		count += nread;
		if (*entry == '#') {
			//printf("comment\n");
			sscanf(gbuffer.data + count, "%*[^\n]%n", &nread);
		}
		else if (strcmp(entry, "res") == 0) {
			//printf("%s\n", entry);
			char string[16];
			nmatches = sscanf (gbuffer.data + count, _"%d"_"%d%n",
				&scene->res.width, &scene->res.height, &nread);
			if (nmatches != 2) {
				nmatches = sscanf(gbuffer.data + count, _"%s%n", string, &nread);
				if (nmatches == 1 && strcmp(string, "fullscreen") == 0) {
					gflag |= FULLSCREEN_ON;
				} else err("invalid parameters for entry '%s' at line %d of file '%s'\n", entry, lineno, scene_filename);
			}
		}
		else if (strcmp(entry, "camera") == 0) {
			//printf("%s\n", entry);
			nmatches = sscanf (gbuffer.data + count,
				_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f%n",
				&scene->cam.eye.x, &scene->cam.eye.y, &scene->cam.eye.z,
				&scene->cam.aim.x, &scene->cam.aim.y, &scene->cam.aim.z,
				&scene->cam.up.x,  &scene->cam.up.y,  &scene->cam.up.z,
				&scene->cam.fov, &scene->cam.focus, &nread);
			if (nmatches != 11) err("invalid parameters for entry '%s' at line %d of file '%s'\n", entry, lineno, scene_filename);
		}
		else if (strcmp(entry, "material") == 0) {
			//printf("%s\n", entry);
			sscanf(gbuffer.data + count, _"%*s%n", &nread);
			scene->material[scene->num_materials].name = malloc(nread + 1);
			if (!scene->material[scene->num_materials].name) err("failed to allocate memory for scene of file '%s': %s\n", scene_filename, strerror(errno));
			nmatches = sscanf (gbuffer.data + count,
				_"%s"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f"_"%f%n",
				scene->material[scene->num_materials].name, &scene->material[scene->num_materials].color.r,
				&scene->material[scene->num_materials].color.g, &scene->material[scene->num_materials].color.b,
				&scene->material[scene->num_materials].index.kd, &scene->material[scene->num_materials].index.ks,
				&scene->material[scene->num_materials].index.ke, &scene->material[scene->num_materials].index.alpha,
				&nread);
			if (nmatches != 8) err("invalid parameters for entry '%s' at line %d of file '%s'\n", entry, lineno, scene_filename);
			scene->num_materials++;
		}
		else if (strcmp(entry, "mesh") == 0) {
			int nread2;
			//printf("%s\n", entry);
			sscanf(gbuffer.data + count, _"%*s%n"_"%*s%n", &nread, &nread2);
			memory = malloc(nread + nread2 + 2);
			if (!memory) err("failed to allocate memory for scene of file '%s': %s\n", scene_filename, strerror(errno));
			scene->model_info[scene->num_models].mesh_filename = memory;
			scene->model_info[scene->num_models].material_name = memory + nread + 1;
			nmatches = sscanf(gbuffer.data + count, " %s %s%n",
				scene->model_info[scene->num_models].mesh_filename,
				scene->model_info[scene->num_models].material_name, &nread);
			if (nmatches != 2) err("invalid parameters for entry '%s' at line %d of file '%s'\n", entry, lineno, scene_filename);
			scene->model_info[scene->num_models].file_line = lineno;
			scene->num_models++;
		}
		else {
			err("entry '%s' at line %d of file '%s' matches none\n", entry, lineno, scene_filename);
		}
		count += nread;
		lineno += count_lines(&count, gbuffer.data);
	}
}

void mesh_from_file (Mesh *mesh, char *mesh_filename) {
	char entry[4];
	int nread, count = 0, nmatches, lineno = 1, input_size;
	char *instances[] = { "v", "vn", "vt", "f" };
	int instance_counters[arrsz(instances)] = {};
	struct { Vect *v, *vn; Vec2 *vt; struct { uint16_t v[3], vn[3], vt[3]; } *f; } input;
	int num_v = 0, num_vn = 0, num_vt = 0, num_f = 0;
	void *memory;

	read_file(&gbuffer, SCENE_DIR, mesh_filename);
	if (gbuffer.count <= 0) err("failed to read file '%s': %s\n", mesh_filename, strerror(errno));

	*mesh = (Mesh){};
	count_instances(instance_counters, instances, arrsz(instances), gbuffer.data);
	input_size = instance_counters[0] * sizeof(Vect) + instance_counters[1] * sizeof(Vect) +
		instance_counters[2] * sizeof(Vec2) + instance_counters[3] * sizeof(typeof(input.f[0]));
	memory = malloc(input_size);
	if (!memory) err("failed to allocate memory for mesh of file '%s'\n", mesh_filename);

	input.v = memory;
	input.vn = memory + instance_counters[0] * sizeof(Vect);
	input.vt = (void *)input.vn + instance_counters[1] * sizeof(Vect);
	input.f = (void *)input.vt + instance_counters[2] * sizeof(Vec2);

	mesh->min_corner = (Vect){ FLT_MAX, FLT_MAX, FLT_MAX };
	mesh->max_corner = (Vect){ -FLT_MAX, -FLT_MAX, -FLT_MAX };

	printf("reading %d bytes from file '%s'\n", gbuffer.count, mesh_filename);
	lineno += count_lines(&count, gbuffer.data);
	while (sscanf(gbuffer.data + count, "%s%n", entry, &nread) > 0) {
		count += nread;
		if (*entry == '#') {
			//printf("#\n");
			sscanf(gbuffer.data + count, "%*[^\n]%n", &nread);
		}
		else if (strcmp(entry, "v") == 0) {
			//printf("v\n");
			nmatches = sscanf(gbuffer.data + count, _"%f"_"%f"_"%f%n",
				&input.v[num_v].x, &input.v[num_v].y, &input.v[num_v].z, &nread);
			if (nmatches != 3) err("invalid parameters for entry '%s' at line %d of file '%s'\n",
				entry, lineno, mesh_filename);

			mesh->baricenter.x += input.v[num_v].x;
			mesh->baricenter.y += input.v[num_v].y;
			mesh->baricenter.z += input.v[num_v].z;

			for (int i = 0; i < 3; i++) {
				if (input.v[num_v].v[i] > mesh->max_corner.v[i])
					mesh->max_corner.v[i] = input.v[num_v].v[i];
				if (input.v[num_v].v[i] < mesh->min_corner.v[i])
					mesh->min_corner.v[i] = input.v[num_v].v[i]; 
			} 
			num_v++;
		}
		else if (strcmp(entry, "vn") == 0) {
			//printf("vn\n");
			nmatches = sscanf(gbuffer.data + count, _"%f"_"%f"_"%f%n",
				&input.vn[num_vn].x, &input.vn[num_vn].y, &input.vn[num_vn].z, &nread);
			if (nmatches != 3)
				err("invalid parameters for entry '%s' at line %d of file '%s'\n", entry, lineno, mesh_filename);
			num_vn++;
		}
		else if (strcmp(entry, "f") == 0) {
			//printf("f\n");
			nmatches = sscanf(gbuffer.data + count, _"%hu//%hu"_"%hu//%hu"_"%hu//%hu%n",
				&input.f[num_f].v[0], &input.f[num_f].vn[0], &input.f[num_f].v[1],
				&input.f[num_f].vn[1], &input.f[num_f].v[2], &input.f[num_f].vn[2], &nread);
			if (nmatches != 6) err("invalid parameters for entry '%s' at line %d of file '%s'\n",
				entry, lineno, mesh_filename); // TODO: change to 9
			num_f++;
		}
		else if (strcmp(entry, "vt") == 0) {
			//printf("vt\n");
			nmatches = sscanf(gbuffer.data + count, _"%f"_"%f%n",
				&input.vt[num_vt].x, &input.vt[num_vt].y, &nread);
			if (nmatches != 2) err("invalid parameters for entry '%s' at line %d of file '%s'\n",
				entry, lineno, mesh_filename);
			num_vt++;
		}
		else if (strcmp(entry, "o") == 0) {
			//printf("o\n");
			sscanf(gbuffer.data + count, "%*[^\n]%n", &nread);
		}
		else if (strcmp(entry, "s") == 0) {
			//printf("s\n");
			sscanf(gbuffer.data + count, "%*[^\n]%n", &nread);
		}
		else {
			err("entry '%s' at file '%s' matches none\n", entry, mesh_filename);
		}
		count += nread;
		lineno += count_lines(&count, gbuffer.data);
	}

	mesh->baricenter = *mul_sv(1.0f/num_v, &mesh->baricenter);
	//printf("mesh '%s' baricenter = { %f, %f, %f }\n", mesh_filename,
		//mesh->baricenter.x, mesh->baricenter.y, mesh->baricenter.z);
	// TODO: sort the face indices to achieve maximum mesh compression for either flat/smooth/mixe normals
	// TODO: implement ebo with num_faces
	// TODO: compress normals and texture coordinates to int16_t and uint16_t
	mesh->vertices.vertex = malloc(3*num_f * (2*sizeof(Vect) + sizeof(Vec2)));
	if (!mesh->vertices.vertex) err("failed to allocate memory for mesh of file '%s'\n", mesh_filename);
	mesh->vertices.normal = (void *)mesh->vertices.vertex + 3*num_f * sizeof(Vect);
	mesh->vertices.texture_coordinate = (void *)mesh->vertices.normal + num_f * sizeof(Vect);

	for (int i = 0; i < num_f; i++) {
		for (int j = 0; j < 3; j++) {
			mesh->vertices.vertex[3*i + j] = input.v[input.f[i].v[j] - 1];
			mesh->vertices.normal[3*i + j] = input.vn[input.f[i].vn[j] - 1];
			// TODO: handle lack of normals and texture coordinates
			//mesh->vertices.texture_coordinate[i] = input.vt[input.f[i].vt - 1];
			//printf("vertices { %9f, %9f, %9f }, normals { %9f, %9f, %9f }\n", mesh->vertices.vertex[i+j].x, mesh->vertices.vertex[i+j].y, mesh->vertices.vertex[i+j].z, mesh->vertices.normal[i+j].x, mesh->vertices.normal[i+j].y, mesh->vertices.normal[i+j].z);
			mesh->num_vertices++;
		}
	}
	//printf("\n");
	//for (int i = 0; i < mesh->num_vertices; i++) {
			//printf("vertices { %9f, %9f, %9f }, normals { %9f, %9f, %9f }\n", mesh->vertices.vertex[i].x, mesh->vertices.vertex[i].y, mesh->vertices.vertex[i].z, mesh->vertices.normal[i].x, mesh->vertices.normal[i].y, mesh->vertices.normal[i].z);

	//}
	free(memory);
}

#undef _

// each element of counters must have the initial counter value of each instance (typically 0)
// the number of valid instances and counters must be the same
void count_instances (int *counters, char **instances, int num_instances, char *text) {
	char entry[32];
	int nread, count = 0, i;

	while (sscanf(text + count, " %s%n", entry, &nread) > 0) {
		count += nread;
		for (i = 0; i < num_instances && strcmp(entry, instances[i]) != 0; i++);
		if (i < num_instances) counters[i]++;
		sscanf(text + count, " %*[^\n]%n", &nread);
		count += nread;
	}
}

// return number of '\n's found and increases character count provided
int count_lines (int *count, char *text) {
	int line_counter = 0, i;
	char c;

	for (i = 0; ; i++) {
		c = text[*count + i];
		if (c == '\n')
			line_counter++;
		else if (c != ' ' && c != '\t' && c != '\r')
			break;
	}

	*count += i;
	return line_counter;
}
