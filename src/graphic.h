#ifndef FCGR_GRAPHIC_HEADER
#define FCGR_GRAPHIC_HEADER

#include <SDL2/SDL.h>
#include "glad.h"
#include "mtrx.h"
#include "utils.h"

#define MAX_ATTRIBUTES 2
#define MAX_UNIFORMS 8
#define MAX_PROGRAMS 3

#define MAX_NAME_LENGTH 16
#define MAX_SCENE_MESHES 5
#define MAX_SCENE_MATERIALS 5

#define MAX_MESH_VERTICES 30
#define MAX_MESH_INDICES (3 * MAX_MESH_VERTICES)

typedef struct Screen	  Screen;
typedef struct Vertex	  Vertex;
typedef struct Mesh		Mesh;
typedef struct Model	   Model;
typedef struct Renderer	Renderer;
typedef struct Viewport	Viewport;
typedef struct Camera	  Camera;
typedef struct MapMesh	 MapMesh;
typedef struct MapVertex   MapVertex;
typedef struct FaceIndices FaceIndices;
typedef struct Vec2		Vec2;

typedef struct Scene	 Scene;
typedef struct Material  Material;
typedef struct ModelInfo ModelInfo;

enum program_index { GEOMETRY_RENDERER, SHADOW_RENDERER, DEPTHMAP_RENDERER };

void create_opengl_context (Screen *screen, Scene *scene, uint32_t flags);
void program_from_shader_source (Renderer *renderer, int program_index, const char *vertsh_file, const char *fragsh_file);
void retrieve_attributes (Renderer *renderer, const char *vertex_attrib_name, const char *fst_edge_attrib_name, const char *snd_edge_attrib_name);
void retrieve_uniforms (Renderer *renderer, int count, ...); // ... = name of uniforms
void retrieve_buffer_objects (Model *model, int num_vaos, ...); // ... = program indices
void vertex_attribute_data (const Model *model, const Renderer *renderer);
void retrieve_framebuffer (Renderer *renderer);
void draw_models (Renderer *renderer, Camera *camera, int num_models, Model *models, Vect *light_source);
GLuint shader_from_sourcefile (GLenum type, const char *path);
void screen_close (Screen *screen);
void scene_from_file (Scene *scene, char *scene_filename);
void mesh_from_file (Mesh *mesh, char *mesh_filename);
Model *models_from_scene (Scene *scene);
void camera_from_scene_and_screen (Camera *camera, Scene *scene, Screen *screen);
void set_opengl_options ();

struct Viewport {
	GLint x, y;
	GLsizei w, h;
};
struct Screen {
	SDL_DisplayMode display;
	SDL_GLContext context;
	SDL_Window *window;
	struct { float w, h; } sizef;
};
// different kinds of vertices data will arrive
struct Vertex {
	struct { float vx, vy, vz; };
	struct { int8_t nx, ny, nz, nw; };
	struct { int16_t rx, ry, rz, rw; };
	struct { int16_t sx, xy, sz, sw; };
};
struct FaceIndices { GLushort v1, v2, v3; };
struct Vec2 { float x, y; };
// so different kinds of meshes shall appear too
struct Mesh {
	struct {
		Vect *vertex;
		Vect *normal;
		Vec2 *texture_coordinate;
	} vertices;
	FaceIndices *face;
	int num_vertices, num_faces;

	Vect baricenter, max_corner, min_corner;
	//struct { Vertex *data; size_t size; } vertices;
	//struct { FaceIndices  *data; size_t size; } indices;
};
struct MapMesh {
	struct { MapVertex *data; size_t size; } vertices;
	struct { FaceIndices *data; size_t size; } indices;
};
struct Material {
	char *name;
	struct { float r, g, b; } color;
	struct { float ke, kd, ks, alpha; } index;
};
struct Model {
	Quat orientation;
	Vect translation;
	Vect dr, ds;
	Vect color;
	Mesh mesh;
	//union { Mesh mesh; MapMesh map; };
	//float shininess;
	//float wabble; // = time.now if wabbling, = -1.0f if not
	Material *material;
	GLuint vbo, ebo;
	GLuint vao[MAX_PROGRAMS];
	float scale;
};
struct Camera {
	Mtrx projm, viewm;
	Vect eye, aim, up;
	Vect dr, ds;
	float focus;
};
struct Renderer {
	int program_index;
	GLuint program, vertex_shader, fragment_shader, framebuffer, fbo;
#if defined(ANDROID) || defined(IOS)
	GLuint rbo; // for shadowmap inactive colorbuffer, which is necessary in OpenGLES
#endif
	struct { GLint vertex, normal, texture_coordinate; } attribute;
	//struct { GLint vertex; union { GLint mapcoord; GLint fst_edge; }; GLint snd_edge; } attribute;
	Viewport viewport;
	GLbitfield clear_flags;
	GLint uniform[MAX_UNIFORMS];
};
struct MapVertex {
	struct { float vx, vy; }; // vertex
	struct { float cx, cy; }; // coordinate
};

struct ModelInfo {
	char *mesh_filename;
	char *material_name;
	int file_line;
};
struct Scene {
	struct { Vect eye, aim, up; float focus, fov; } cam;
	struct { int width, height; } res;
	int num_materials, num_models;
	Material *material;
	ModelInfo *model_info;
	char *filename;
};

#endif // FCGR_GRAPHIC_HEADER
