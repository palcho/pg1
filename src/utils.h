#ifndef FCGR_UTILS_HEADER
#define FCGR_UTILS_HEADER

#define DEBUG_VERSION
//#define LINUX
#define WINDOWS
//#define ANDROID
//#define MAC
//#define IOS

#define SCENE_DIR "scenes/"
#define SHADER_DIR "opengl/"

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <SDL2/SDL.h>

#define assertw(expr) if (!(expr)) { report_error(__FILE__, #expr, __LINE__, GL_NO_ERROR); }
#define assertq(expr) if (!(expr)) { report_error(__FILE__, #expr, -__LINE__, GL_NO_ERROR); }
#define asserth(expr, error_handler) if (!(expr)) {\
	report_error(__FILE__, #expr, __LINE__, GL_NO_ERROR);\
	goto error_handler;\
}

#ifdef DEBUG_VERSION
#   define GL(line) do {\
		line;\
		GLint opengl_error = glGetError();\
		if (opengl_error != GL_NO_ERROR)\
			report_error(__FILE__, #line, __LINE__, opengl_error);\
	} while (0)
#else
#   define GL(line) line
#endif

#define err(...) do {\
	fprintf(stderr, "error: " __VA_ARGS__);\
	exit(-1);\
} while (0)

#define warn(...) fprintf(stderr, "warning: " __VA_ARGS__);
#define arrsz(x) (sizeof(x)/sizeof((x)[0]))

// structs prototypes
typedef struct Buffer Buffer;
typedef struct Timer Timer;

// function prototypes
void report_error (const char *file_string, const char *line_string, int line_number, int opengl_error);
Buffer alloc_buffer (size_t size);
Buffer realloc_buffer (Buffer buffer, size_t size);
int read_file (Buffer *buffer, const char *prefix, const char *filename);
int write_file (char *buffer, int nwrite, SDL_RWops *file);
void update_timer (Timer *timer);
//int printn (const char *format, ...); // from: network.c

struct Buffer { char *data; int count, size; };
struct Timer { uint32_t now, last_frame, last_sec,  dt, fps/*, last_reached_server, last_scan*/; };

enum global_flag {
	QUIT               = 0x01,
	HAS_VAO            = 0x02,
	OPENGL_INITIALIZED = 0x04,
	JOYSTICK_PLUGGED   = 0x08,
	VSYNC_ON           = 0x10,
	SHOW_FPS           = 0x20,
	HAS_TERMINAL       = 0x40,
	FULLSCREEN_ON      = 0x80,
	ALLOW_BROADCAST    = 0x0100,
	ENABLE_PRINTNET    = 0x0200,
	SERVER_ANSWERED    = 0x0400,
	BACKGROUND         = 0x0800,
	RESTORE_SESSION    = 0x1000,
	SELECT_MODEL       = 0x2000,
	MOVING_MODEL       = 0x4000,
	WALKING_MODEL      = 0x8000,
};
enum joy_axes { LH, LV, LT, RH, RV, RT };
enum joy_buttons { A, B, X, Y, LB, RB, BACK, START, XBOX, LA, RA };

extern Buffer gbuffer;
extern uint32_t gflag;

#endif // FCGR_UTILS_HEADER
