#ifndef FCGR_MTRX_HEADER
#define FCGR_MTRX_HEADER

#define PI 3.14159265f
#define ALIGN __attribute__((aligned(16)))

// structs prototypes
typedef union Vect Vect;
typedef union Vec4 Vec4;
typedef union Quat Quat;
typedef union Mtrx Mtrx;

// function prototypes
Vect *add_vv (const Vect *restrict su, const Vect *restrict du); // add_vv
Vect *sub_vv (const Vect *restrict su, const Vect *restrict du); // sub_vv = su - du
Vect *mul_sv (float s, const Vect *restrict du); // mull_sv
float dot (const Vect *restrict su, const Vect *restrict du); // dot
Vect *cross (const Vect *restrict su, const Vect *restrict du); // cross
Vect *inv_vect (const Vect *restrict su); // inv_vec
Quat *inv_quat (const Quat *restrict sq); // inv_qt
Quat *mul_qq (const Quat *restrict sq, const Quat *restrict dq); // mul_qq
Quat *quat_from_vect (const Vect *restrict du, float w); // vec2qt
Vect *unit_vect (const Vect *restrict du); // norm_vec
Quat *unit_quat (const Quat *restrict dq); // norm_qt
Mtrx *mtrx_from_quat (const Quat *restrict sq); // qt2_mtrx
Mtrx *rotm_from_quat (const Quat *restrict sq); // qt2rotm
Vect *mul_qviq (const Quat *restrict sq, const Vect *restrict du); // mul_qviq
Mtrx *mul_mm (const Mtrx *restrict sm, const Mtrx *restrict dm); // mul_mm
Mtrx *view_mtrx (const Vect *restrict eye, const Vect *restrict aim, const Vect *restrict upp);
Mtrx *persp_mtrx (float anear, float afar, float fov, float ratio);
Mtrx *ortho_mtrx (float anear, float afar, float width, float height);
Mtrx *translate (const Vect *restrict su, Mtrx *restrict sm);
Vect euler_angles (const Quat *restrict sq);
float det (const Mtrx *restrict m);
Vec4 *mul_mv4 (const Mtrx *restrict sm, const Vec4 *restrict du);
Vec4 *mul_mv3 (const Mtrx *restrict sm, const Vect *restrict du);
#define mul_mv(sm, du) ((Vect *)mul_mv3(sm, du))
//Vect *mul_mv (const Mtrx *restrict sm, const Vect *restrict du); // mul_mv

union Vect {
	struct { float x, y, z; };
	float v[3];
};

union ALIGN Vec4 {
	struct {
		union {
			Vect u;
			struct { float x, y, z; };
		};
		float w;
	};
	float v[4];
};

union ALIGN Quat {
	struct {
		union {
			Vect u;
			struct { float x, y, z; };
			float v[3];
		};
		float w;
	};
	float q[4];
};

union ALIGN Mtrx {
	struct { Quat qa, qb, qc, qt; };
	struct {
		Vect ua; float wa;
		Vect ub; float wb;
		Vect uc; float wc;
		Vect ut; float wt;
	};
	struct {
		float rx, ux, fx, nx;
		float ry, uy, fy, ny;
		float rz, uz, fz, nz;
		float tx, ty, tz, tw;
	};
	float m[16];
};

#endif // FCGR_MTRX_HEADER
