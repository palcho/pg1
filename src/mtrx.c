#include "mtrx.h"
#include <math.h>

#define alloc_vect() (Vect *)alloc(sizeof(Vec4))
#define alloc_vec4() (Vec4 *)alloc(sizeof(Vec4))
#define alloc_quat() (Quat *)alloc(sizeof(Quat))
#define alloc_mtrx() (Mtrx *)alloc(sizeof(Mtrx))
inline float *alloc (int size);



Vect *add_vv (const Vect *restrict su, const Vect *restrict du) {
	Vect *v = alloc_vect();
	v->x = su->x + du->x;
	v->y = su->y + du->y;
	v->z = su->z + du->z;
	return v;
}

Vect *sub_vv (const Vect *restrict su, const Vect *restrict du) {
	Vect *v = alloc_vect();
	v->x = su->x - du->x;
	v->y = su->y - du->y;
	v->z = su->z - du->z;
	return v;
}

Vect *mul_sv (float s, const Vect *restrict du) {
	Vect *v = alloc_vect();
	v->x = s * du->x;
	v->y = s * du->y;
	v->z = s * du->z;
	return v;
}

float dot (const Vect *restrict su, const Vect *restrict du) {
	return su->x * du->x  +  su->y * du->y  +  su->z * du->z;
}

Vect *cross (const Vect *restrict su, const Vect *restrict du) {
	Vect *v = alloc_vect();
	v->x = su->y * du->z  -  su->z * du->y;
	v->y = su->z * du->x  -  su->x * du->z;
	v->z = su->x * du->y  -  su->y * du->x;
	return v;
}

Vect *inv_vect (const Vect *restrict su) {
	Vect *v = alloc_vect();
	v->x = -su->x;
	v->y = -su->y;
	v->z = -su->z;
	return v;
}

Quat *inv_quat (const Quat *restrict sq) {
	Quat *q = alloc_quat();
	q->x = -sq->x;
	q->y = -sq->y;
	q->z = -sq->z;
	q->w = sq->w;
	return q;
}

Quat *mul_qq (const Quat *restrict sq, const Quat *restrict dq) {
	Quat *q = alloc_quat();
	q->x = (sq->w * dq->x - sq->z * dq->y) + (sq->y * dq->z + sq->x * dq->w);
	q->y = (sq->z * dq->x + sq->w * dq->y) + (sq->y * dq->w - sq->x * dq->z);
	q->z = (sq->x * dq->y - sq->y * dq->x) + (sq->w * dq->z + sq->z * dq->w);
	q->w = (sq->w * dq->w - sq->z * dq->z) - (sq->x * dq->x + sq->y * dq->y);
	return q;
}

Quat *quat_from_vect (const Vect *restrict du, float w) {
	Quat *q = alloc_quat();
	w *= 0.5f;
	float sinw = sinf(w);
	q->x = du->x * sinw;
	q->y = du->y * sinw;
	q->z = du->z * sinw;
	q->w = cosf(w);
	return q;
}

Vect *unit_vect (const Vect *restrict du) {
	Vect *v = alloc_vect();
	float inv_magn = 1.0f / sqrtf(du->x * du->x + du->y * du->y + du->z * du->z);
	v->x = du->x * inv_magn;
	v->y = du->y * inv_magn;
	v->z = du->z * inv_magn;
	return v;
}

Quat *unit_quat (const Quat *restrict dq) {
	Quat *q = alloc_quat();
	float inv_magn = 1.0f / sqrtf((dq->x * dq->x + dq->y * dq->y) + (dq->z * dq->z + dq->w * dq->w));
	q->x = dq->x * inv_magn;
	q->y = dq->y * inv_magn;
	q->z = dq->z * inv_magn;
	q->w = dq->w * inv_magn;
	return q;
}

Mtrx *mtrx_from_quat (const Quat *restrict sq) {
	Mtrx *m = alloc_mtrx();
	m->m[6] = m->m[12] = sq->x;
	m->m[3] = m->m[9] = -sq->x;
	m->m[8] = m->m[13] = sq->y;
	m->m[2] = m->m[7] = -sq->y;
	m->m[1] = m->m[14] = sq->z;
	m->m[4] = m->m[11] = -sq->z;
	m->m[0] = m->m[5] = m->m[10] = m->m[15] = sq->w;
	return m;
}

Mtrx *rotm_from_quat (const Quat *restrict sq) {
	Mtrx *m = alloc_mtrx();
	*m = (Mtrx) {
		.m[0] = 1.0f - (sq->y * sq->y + sq->z * sq->z) * 2.0f,
		.m[1] = (sq->x * sq->y + sq->w * sq->z) * 2.0f,
		.m[2] = (sq->x * sq->z - sq->w * sq->y) * 2.0f,
		.m[4] = (sq->x * sq->y - sq->w * sq->z) * 2.0f,
		.m[5] = 1.0f - (sq->x * sq->x + sq->z * sq->z) * 2.0f,
		.m[6] = (sq->y * sq->z + sq->w * sq->x) * 2.0f,
		.m[8] = (sq->x * sq->z + sq->w * sq->y) * 2.0f,
		.m[9] = (sq->y * sq->z - sq->w * sq->x) * 2.0f,
		.m[10] = 1.0f - (sq->x * sq->x + sq->y * sq->y) * 2.0f,
		.m[15] = 1.0f
	};
	return m;
}

Vect *mul_qviq (const Quat *restrict sq, const Vect *restrict du) {
	Vect *v = alloc_vect();
	v->x = (1.0f - 2.0f*(sq->y * sq->y + sq->z * sq->z)) * du->x + 2.0f*(sq->x * sq->y - sq->w * sq->z) * du->y + 2.0f*(sq->x * sq->z + sq->w * sq->y) * du->z;
	v->y = 2.0f*(sq->x * sq->y + sq->w * sq->z) * du->x + (1.0f - 2.0f*(sq->x * sq->x + sq->z * sq->z)) * du->y + 2.0f*(sq->y * sq->z - sq->w * sq->x) * du->z;
	v->z = 2.0f*(sq->x * sq->z - sq->w * sq->y) * du->x + 2.0f*(sq->y * sq->z + sq->w * sq->x) * du->y + (1.0f - 2.0f*(sq->x * sq->x + sq->y * sq->y)) * du->z;
	return v;
}

/*
Vect *mul_mv (const Mtrx *restrict sm, const Vect *restrict du) {
	Vect *v = alloc_vect();
	for (int i = 0; i < 4; ++i)
		v->v[i] = (du->x * sm->m[i] + du->y * sm->m[i+4]) + (du->z * sm->m[i+8] + 1.0f * sm->m[i+12]);
	return v;
}
*/

Vec4 *mul_mv4 (const Mtrx *restrict sm, const Vec4 *restrict du) {
	Vec4 *v = alloc_vec4();
	for (int i = 0; i < 4; ++i)
		v->v[i] = (du->x * sm->m[i] + du->y * sm->m[i+4]) + (du->z * sm->m[i+8] + du->w * sm->m[i+12]);
	return v;
}

Vec4 *mul_mv3 (const Mtrx *restrict sm, const Vect *restrict du) {
	Vec4 *v = alloc_vec4();
	for (int i = 0; i < 4; ++i)
		v->v[i] = (du->x * sm->m[i] + du->y * sm->m[i+4]) + (du->z * sm->m[i+8] + 1.0f * sm->m[i+12]);
	return v;
}

Mtrx *mul_mm (const Mtrx *restrict sm, const Mtrx *restrict dm) {
	Mtrx *m = alloc_mtrx();
	for (int i = 0; i < 16; i += 4) {
		for (int j = 0; j < 4; ++j)
			m->m[i+j] = (dm->m[i]*sm->m[j] + dm->m[i+1]*sm->m[j+4]) + (dm->m[i+2]*sm->m[j+8] + dm->m[i+3]*sm->m[j+12]);
	}
	return m;
}

Mtrx *view_mtrx (const Vect *restrict eye, const Vect *restrict aim, const Vect *restrict upp) {
	Mtrx *view = alloc_mtrx();
	Vect back, right, upper;
	
	back = *unit_vect(sub_vv(eye, aim));
	right = *unit_vect(cross(upp, &back));
	upper = *unit_vect(cross(&back, &right));

	*view = (Mtrx) {
		.rx = right.x,
		.ry = right.y,
		.rz = right.z,
		.ux = upper.x,
		.uy = upper.y,
		.uz = upper.z,
		.fx = back.x,
		.fy = back.y,
		.fz = back.z,
		.tx = -dot(&right, eye),
		.ty = -dot(&upper, eye),
		.tz = -dot(&back, eye),
		.tw = 1.0f
	};
	return view;
}

Mtrx *persp_mtrx (float anear, float afar, float fov, float ratio) {
	Mtrx *proj = alloc_mtrx();
	register float height, width;

	height = tanf(fov * 0.5f) * anear; // half height
	width = height * ratio; // half width
	
	*proj = (Mtrx) {
		.rx = anear / width,
		.uy = anear / height,
		.fz = (afar + anear) / (anear - afar),
		.nz = -1.0f,
		.tz = (2.0f * afar * anear) / (anear - afar)
	};
	return proj;
}

Mtrx *ortho_mtrx (float anear, float afar, float width, float height) {
	Mtrx *proj = alloc_mtrx();
	*proj = (Mtrx) {
		.rx = 2.0f / width,
		.uy = 2.0f / height,
		.fz = -2.0f / (afar - anear),
		.tz = -(afar + anear) / (afar - anear),
		.tw = 1.0f
	};
	return proj;
}

float det (const Mtrx *m) {
	register float sum;
	sum = m->m[0] * (m->m[5] * (m->m[10]*m->m[15] - m->m[14]*m->m[11]) - m->m[9] * (m->m[6]*m->m[15] - m->m[14]*m->m[7]) + m->m[13] * (m->m[6]*m->m[11] - m->m[10]*m->m[7]));
	sum -= m->m[4] * (m->m[1] * (m->m[10]*m->m[15] - m->m[14]*m->m[11]) - m->m[9] * (m->m[2]*m->m[15] - m->m[14]*m->m[3]) + m->m[13] * (m->m[2]*m->m[11] - m->m[10]*m->m[3]));
	sum += m->m[8] * (m->m[1] * (m->m[6]*m->m[15] - m->m[14]*m->m[7]) - m->m[5] * (m->m[2]*m->m[15] - m->m[14]*m->m[3]) + m->m[13] * (m->m[2]*m->m[7] - m->m[6]*m->m[3]));
	sum -= m->m[12] * (m->m[1] * (m->m[6]*m->m[11] - m->m[10]*m->m[7]) - m->m[5] * (m->m[2]*m->m[11] - m->m[10]*m->m[3]) + m->m[9] * (m->m[2]*m->m[7] - m->m[6]*m->m[3]));
	return sum;
}

Vect euler_angles (const Quat *restrict sq) {
	Vect angles;
	register float *roll, *pitch, *yaw, rad2deg;
	roll = &angles.x; pitch = &angles.y; yaw = &angles.z;
	rad2deg = 180.0f/PI;
	*roll = rad2deg * atan2f(2.0f * (sq->q[3] * sq->q[0] + sq->q[1] * sq->q[2]), 1.0f - 2.0f * (sq->q[0] * sq->q[0] + sq->q[1] * sq->q[1]));
	*pitch = rad2deg * asinf(fmaxf(-1.0f, fminf(1.0f, 2.0f * (sq->q[3] * sq->q[1] - sq->q[2] * sq->q[0]))));
	*yaw = rad2deg * atan2f(2.0f * (sq->q[3] * sq->q[2] + sq->q[0] * sq->q[1]), 1.0f - 2.0f * (sq->q[1] * sq->q[1] + sq->q[2] * sq->q[2]));
	return angles;
}

Mtrx *translate (const Vect *restrict su, Mtrx *restrict dm) {
	Mtrx *m = alloc_mtrx();
	*m = *dm;
	m->ut.x += su->x;
	m->ut.y += su->y;
	m->ut.z += su->z;
	return m;
}

// local helpers
#define MTRX_BUFFER_SIZE (8 * sizeof(Mtrx)) // must be a power of 2

ALIGN float mtrx_buffer[(MTRX_BUFFER_SIZE + sizeof(Mtrx)) / sizeof(float)];
int mtrx_index;

inline float *alloc (int size) {
	register void *result = (void *)mtrx_buffer + mtrx_index;
	mtrx_index = (mtrx_index + size) & (MTRX_BUFFER_SIZE - 1);
	return result;
}
