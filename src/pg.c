#include <string.h>
#include <errno.h>
#include "graphic.h"
#include "mtrx.h"
#include "utils.h"
#include "mechanics.h"

int main (int argc, char **argv) {
	Scene scene;
	Screen screen;
	Timer timer = {};
	Renderer geometry_renderer/*, shadow_renderer*/;
	Model *object_models;
	Camera camera;
	Vect light_pos = { 1.0f, 2.0f, -3.0f };

	// global buffer and timer
	gbuffer = alloc_buffer(1408);
	if (!gbuffer.data) err("failed to allocate global buffer: %s\n", strerror(errno));
	timer.now = timer.last_frame = timer.last_sec = SDL_GetTicks();

	// reading scene file and creating windows
	scene_from_file(&scene, "world.scn");
	object_models = models_from_scene(&scene);
	create_opengl_context(&screen, &scene, VSYNC_ON);
	camera_from_scene_and_screen(&camera, &scene, &screen);

	// geometry renderer
	geometry_renderer.viewport = (Viewport) { .w = screen.display.w, .h = screen.display.h };
	program_from_shader_source(&geometry_renderer, GEOMETRY_RENDERER,
		"geometry.vsh", "geometry.fsh");
	retrieve_attributes(&geometry_renderer, "vertex", "normal", NULL);
	retrieve_uniforms(&geometry_renderer, 8, "vertex_mtrx", "normal_mtrx", "view_mtrx",
		"proj_mtrx", "object_color", "light_pos", "material", "view_pos");
	geometry_renderer.clear_flags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;

	// objects models
	for (int i = 0; i < scene.num_models; i++) {
		retrieve_buffer_objects(&object_models[i], 1, geometry_renderer.program_index);
		vertex_attribute_data(&object_models[i], &geometry_renderer);
	}
	set_opengl_options();

	while (~gflag & QUIT) {
		update_timer(&timer);
		read_inputs(&timer);
		update_camera_position(&camera, &timer);
		draw_models(&geometry_renderer, &camera, scene.num_models, object_models, &light_pos);
		SDL_GL_SwapWindow(screen.window);
	}

	screen_close(&screen);
	return 0;
}
