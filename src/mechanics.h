#ifndef FCGR_MECHANICS_HEADER
#define FCGR_MECHANICS_HEADER

#include "graphic.h"
#include "utils.h"

void read_inputs (Timer *timer);
void update_camera_position (Camera *camera, Timer *timer);

extern Model *grabbed_model;

#endif // FCGR_MECHANICS_HEADER
