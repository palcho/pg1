#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "glad.h"
#include "utils.h"

Buffer gbuffer = { NULL, 0, 0 };
#if defined(ANDROID) || defined(IOS)
uint32_t gflag = FULLSCREEN_ON; // global flag
#elif defined(LINUX) || defined(WINDOWS) || defined(MAC)
uint32_t gflag = HAS_TERMINAL;
#endif

void report_error (const char *file_string, const char *line_string, int line_number, int opengl_error) {
	size_t size = 435, length = 0;
	char buffer[size]; // to have it's own local buffer is safer
	length = snprintf(buffer, size, "ERROR in file %s, line %d: \"%s\":\n  SDL2: %s\n  errno %d: %s\n",\
		file_string, abs(line_number), line_string, SDL_GetError(), errno, strerror(errno));
	if (gflag & OPENGL_INITIALIZED) {
		length += snprintf(buffer + length, size - length, "  OpenGL: 0x%04x\n",\
			opengl_error ? opengl_error : glGetError());
	}
	buffer[length] = '\0';
	if (gflag & HAS_TERMINAL) {
		fwrite(buffer, sizeof(char), length, stderr);
	} else {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR", buffer, NULL);
	}
	if (line_number < 0) {
		exit(line_number);
	}
}

int read_file (Buffer *buffer, const char *prefix, const char *filename) {
	char name_buffer[256];
	char *ptr = name_buffer, *temp_ptr;
	int64_t file_size;
	SDL_RWops *file;

	if (prefix) {
		while ((*ptr++ = *prefix++));
		ptr--;
	}
	while ((*ptr++ = *filename++));

	file = SDL_RWFromFile(name_buffer, "r");
	if (!file) goto file_open_error;
	file_size = SDL_RWsize(file);

	if (file_size < 0) { // could not get file size
		buffer->count = SDL_RWread(file, buffer->data, sizeof(char), buffer->size);
		while (buffer->count == buffer->size) {
			*buffer = realloc_buffer(*buffer, buffer->size * 2);
			if (buffer->size != buffer->count) goto buffer_realloc_error;
			temp_ptr = buffer->data + buffer->count;
			buffer->count += SDL_RWread(file, temp_ptr, sizeof(char), buffer->size/2);
		}
	} else { // got file size
		if (file_size > buffer->size) {
			*buffer = realloc_buffer(*buffer, file_size+1);
			if (buffer->size < file_size) goto buffer_realloc_error;
		}
		buffer->count = SDL_RWread(file, buffer->data, sizeof(char), buffer->size);
	}

	SDL_RWclose(file);
	buffer->data[buffer->count] = '\0';
	return buffer->count;

buffer_realloc_error:
	SDL_RWclose(file);
file_open_error:
	buffer->count = 0;
	return buffer->count;
}

int write_file (char *buffer, int nwrite, SDL_RWops *file) {
	asserth(file != NULL, write_file_open_error);
	nwrite = SDL_RWwrite(file, buffer, nwrite, 1);
	SDL_RWclose(file);
	return nwrite;
write_file_open_error:
	return 0;
}

Buffer alloc_buffer (size_t size) {
	Buffer buffer;
	buffer.data = malloc(size);
	buffer.count = 0;
	buffer.size = buffer.data ? size : 0;
	return buffer;
}

Buffer realloc_buffer (Buffer buffer, size_t new_size) {
	char *tmp_buffer = realloc(buffer.data, new_size);
	if (tmp_buffer != NULL) {
		buffer = (Buffer){ tmp_buffer, buffer.count, new_size };
	}
	return buffer;
}

void update_timer (Timer *timer) {
	timer->last_frame = timer->now;
	timer->now = SDL_GetTicks();
	timer->dt = timer->now - timer->last_frame;
	timer->fps++;
	if (timer->now - timer->last_sec >= 1000) {
		if (gflag & SHOW_FPS)
			printf("fps = %u\n", timer->fps);
		timer->last_sec = timer->now;
		timer->fps = 0;
	}
}
